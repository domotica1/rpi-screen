import time
import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

import subprocess

# Raspberry Pi pin configuration:
RST = None     # on the PiOLED this pin isnt used

# Note the following are only used with SPI:
DC = 23
SPI_PORT = 0
SPI_DEVICE = 0

# 128x64 display with hardware I2C:
disp = Adafruit_SSD1306.SSD1306_128_64(rst=RST)

# Initialize library.
disp.begin()

# Clear display.
disp.clear()
disp.display()

# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
width = disp.width
height = disp.height
image = Image.new('1', (width, height))

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)

# Draw a black filled box to clear the image.
draw.rectangle((0,0,width,height), outline=0, fill=0)

# Draw some shapes.
# First define some constants to allow easy resizing of shapes.
padding = -2
top = padding
bottom = height-padding
# Move left to right keeping track of the current x position for drawing shapes.
x = 0

# Load default font.
font = ImageFont.load_default()

# Alternatively load a TTF font.  Make sure the .ttf font file is in the same directory as the python script!
# Some other nice fonts to try: http://www.dafont.com/bitmap.php
font = ImageFont.truetype('Montserrat-Light.ttf', 13)
font2 = ImageFont.truetype('fontawesome-webfont.ttf', 14)
font_text_big = ImageFont.truetype('Montserrat-Medium.ttf', 19)
font_icon_big = ImageFont.truetype('fontawesome-webfont.ttf', 20)

while True:

    # Draw a black filled box to clear the image.
    draw.rectangle((0,0,width,height), outline=0, fill=0)

    # Shell scripts for system monitoring from here : https://unix.stackexchange.com/questions/119126/command-to-display-memory-usage-disk-usage-and-cpu-load
    cmd = "ip -4 addr show eth0 | grep -oP '(?<=inet\s)\d+(\.\d+){3}'"
    Show_IP = subprocess.check_output(cmd, shell = True )
    cmd = "top -bn1 | grep load | awk '{printf \"%.1f\", $(NF-2)}'"
    Show_CPU = subprocess.check_output(cmd, shell = True )
    cmd = "free -m | awk 'NR==2{printf \"%.0f%%\", $3*100/$2 }'"
    Show_MemUsage = subprocess.check_output(cmd, shell = True )
    cmd =  "df -h /dev/sda1 | awk '{print $5}' | tail -1 |head --bytes -1"
    Show_DiskNAS = subprocess.check_output(cmd, shell = True )
    cmd = "df -h | awk '$NF==\"/\"{printf \"%s\", $5}'"
    Show_DiskSD = subprocess.check_output(cmd, shell = True )
    cmd = "vcgencmd measure_temp | cut -d '=' -f 2 | cut -d'.' -f1 |head --bytes -1"
    Show_Temperature = subprocess.check_output(cmd, shell = True )

    
    # Icons
    #draw.text((x, top+3),     chr(63231),  font=font2, fill=255)            #Network
    #draw.text((x, top+17),    chr(62171),  font=font2, fill=255)            #CPU
    #draw.text((x, top+37),    chr(62776),  font=font2, fill=255)            #Mem
    #draw.text((x+55, top+37), chr(62152),  font=font2, fill=255)            #Temp
    #draw.text((x, top+52),    chr(63426),  font=font2, fill=255)            #SD
    #draw.text((x+55, top+52), chr(63426),  font=font2, fill=255)            #NAS

    draw.text((x+0, top+0), "NAS STATS", font=ImageFont.truetype('Montserrat-Light.ttf', 16), fill=255)
    draw.text((x+2, top+15), "IP: " + str(Show_IP,           'UTF-8'), font=font, fill=255)
    draw.text((x+2, top+32), "CPU: " + str(Show_CPU,          'UTF-8'), font=font, fill=255)
    draw.text((x+65, top+32), "MEM: " + str(Show_MemUsage,     'UTF-8'), font=font, fill=255)
    draw.text((x+95, top+1), str(Show_Temperature,  'UTF-8') + "º", font=ImageFont.truetype('Montserrat-Light.ttf', 14), fill=255)
    draw.text((x+2, top+50), "SD: " + str(Show_DiskSD,       'UTF-8'), font=font, fill=255)
    draw.text((x+65, top+50), "NAS: " + str(Show_DiskNAS,      'UTF-8'), font=font, fill=255)

    # Display image.
    disp.image(image)
    disp.display()
    time.sleep(.1)
